/* Another way get max and min floor number
    * currently unused
 */
package models;

public final class FloorsCount {
    private static final String STATE_EXCEPTION = "FloorsCount class";

    private FloorsCount() {
        throw new IllegalStateException(STATE_EXCEPTION);
    }

    public static final int MAX_FLOORS = 100;
    public static final int MIN_FLOOR = 1;
}
