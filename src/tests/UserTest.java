package tests;

import builders.LiftBuilder;
import builders.UserBuilder;
import models.Direction;
import models.Lift;
import models.TestTimingWriter;
import models.User;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import static org.testng.Assert.*;

@Listeners(value = TestTimingWriter.class)
public class UserTest {
    private static Logger logger = Logger.getLogger("Logging Exception");

    private static void logException(Exception e) {
        StringWriter trace = new StringWriter();
        e.printStackTrace(new PrintWriter(trace));
        String msg = trace.toString();
        logger.severe(msg);
    }

    @DataProvider(name = "userAndLiftsProvider")
    public Object[][] userAndLiftsProvider() {
        List<Lift> lifts1 = null;
        List<Lift> lifts2 = null;
        List<Lift> lifts3 = null;
        List<Lift> lifts4 = null;
        List<Lift> lifts5 = null;
        List<Lift> lifts6 = null;
        List<Lift> lifts7 = null;

        User user1 = null;
        User user2 = null;
        User user3 = null;
        User user4 = null;
        User user5 = null;
        User user6 = null;
        User user7 = null;

        try {
            lifts1 = new ArrayList<>(Arrays.asList(
                    new LiftBuilder().setCurrentFloor(100).setFinishFloor(2).build(),
                    new LiftBuilder().setCurrentFloor(1).setFinishFloor(2).build()
            ));
            Lift.resetCounterId();
            lifts2 = new ArrayList<>(Arrays.asList(
                    new LiftBuilder().setCurrentFloor(100).setFinishFloor(2).build(),
                    new LiftBuilder().setCurrentFloor(5).setFinishFloor(3).build(),
                    new LiftBuilder().setCurrentFloor(7).setFinishFloor(4).build()
            ));
            Lift.resetCounterId();
            lifts3 = new ArrayList<>(Arrays.asList(
                    new LiftBuilder().setCurrentFloor(100).setFinishFloor(2).build(),
                    new LiftBuilder().setCurrentFloor(5).setFinishFloor(4).build(),
                    new LiftBuilder().setCurrentFloor(2).setFinishFloor(4).build()
            ));
            Lift.resetCounterId();
            lifts4 = new ArrayList<>(Arrays.asList(
                    new LiftBuilder().setCurrentFloor(100).setFinishFloor(99).build(),
                    new LiftBuilder().setCurrentFloor(2).setFinishFloor(96).build(),
                    new LiftBuilder().setCurrentFloor(95).setFinishFloor(94).build(),
                    new LiftBuilder().setCurrentFloor(10).setFinishFloor(1).build()
            ));
            Lift.resetCounterId();
            lifts5 = new ArrayList<>(Arrays.asList(
                    new LiftBuilder().setCurrentFloor(2).setFinishFloor(2).build(),
                    new LiftBuilder().setCurrentFloor(6).setFinishFloor(3).build(),
                    new LiftBuilder().setCurrentFloor(4).setFinishFloor(2).build()
            ));
            Lift.resetCounterId();
            lifts6 = new ArrayList<>(Arrays.asList(
                    new LiftBuilder().setCurrentFloor(7).setFinishFloor(9).build(),
                    new LiftBuilder().setCurrentFloor(3).setFinishFloor(4).build(),
                    new LiftBuilder().setCurrentFloor(10).setFinishFloor(5).build()
            ));
            Lift.resetCounterId();
            lifts7 = new ArrayList<>(Arrays.asList(
                    new LiftBuilder().setCurrentFloor(7).setFinishFloor(9).build(),
                    new LiftBuilder().setCurrentFloor(6).setFinishFloor(8).build(),
                    new LiftBuilder().setCurrentFloor(2).setFinishFloor(3).build(),
                    new LiftBuilder().setCurrentFloor(4).setFinishFloor(3).build()
            ));
            Lift.resetCounterId();


            user1 = new UserBuilder().setCurrentFloor(3).setUserDirection(Direction.UP).build();
            user2 = new UserBuilder().setCurrentFloor(4).setUserDirection(Direction.DOWN).build();
            user3 = new UserBuilder().setCurrentFloor(4).setUserDirection(Direction.DOWN).build();
            user4 = new UserBuilder().setCurrentFloor(96).setUserDirection(Direction.DOWN).build();
            user5 = new UserBuilder().setCurrentFloor(5).setUserDirection(Direction.DOWN).build();
            user6 = new UserBuilder().setCurrentFloor(5).setUserDirection(Direction.UP).build();
            user7 = new UserBuilder().setCurrentFloor(5).setUserDirection(Direction.UP).build();

        } catch (IllegalArgumentException e) {
            logException(e);
        }

        return new Object[][] {
                {lifts1, user1, 2},
                {lifts2, user2, 2},
                {lifts3, user3, 2},
                {lifts4, user4, 2},
                {lifts5, user5, 2},
                {lifts6, user6, 3},
                {lifts7, user7, 3}
        };
    }

    @Test(dataProvider = "userAndLiftsProvider")
    public void test(List<Lift> lifts, User user, int expId) {
        assertEquals(user.getBestLift(lifts).getId(), expId);
    }

}